﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimTp3
{
    public partial class frmResultado : Form
    {
        private int? _param2;
        private int _cantValores, _cantIntervalos;
        private string _distribucion;
        private Random rnd = new Random();
        private double _maxValor, _minValor, _param1;
        private double[] frecEsperada, frecObservada;
        private double[] vectorChi = new double[30];

        public frmResultado()
        {
            InitializeComponent();
        }

        public frmResultado(double param1, int? param2, int cantValores, int cantIntervalos, string distribucion)
        {
            _param1 = param1; _param2 = param2; _cantValores = cantValores; _cantIntervalos = cantIntervalos;
            _distribucion = distribucion;
            InitializeComponent();
            _maxValor = -10000000000000;
            _minValor = 1000000000000000;

            cargarVectorChi();
            GenerateData();
        }


        private void GenerateData()
        {

            switch (_distribucion)
            {
                case "uniforme":
                    DistribucionUniforme();
                    break;
                case "normal":
                    Normal(_param1, (double)_param2);
                    break;
                case "exponencial":
                    ExpNegativa();
                    break;
                case "poisson":
                    Poisson();
                    break;
            }

            frecObservada = new double[_cantIntervalos];
            frecEsperada = new double[_cantIntervalos];

            double anchoIntervalo = Math.Round((_maxValor - _minValor) / _cantIntervalos, 4);

            int index = 0;

            while (index < _cantIntervalos)
            {
                frecObservada[index] = 0;
                frecEsperada[index] = 0;
                index += 1;
            }

            double valorActual;

            if (_distribucion != "poisson")
            {
                int auxiliar;
                foreach (DataGridViewRow row in dgvValores.Rows)
                {
                    valorActual = Math.Round(Convert.ToDouble(row.Cells[0].Value) - _minValor, 4);
                    auxiliar = 0;

                    while (valorActual > anchoIntervalo)
                    {
                        valorActual = Math.Round(valorActual - anchoIntervalo, 4);
                        auxiliar += 1;
                    }

                    if (auxiliar == _cantIntervalos)
                        frecObservada[auxiliar - 1] += 1;
                    else
                        frecObservada[auxiliar] += 1;
                }
            }
            else
            {

                frecEsperada = new double[_cantIntervalos + 1];
                frecObservada = new double[_cantIntervalos + 1];

                for (index = 0; index < _cantIntervalos; index++)
                {
                    frecEsperada[index] = 0;
                    frecObservada[index] = 0;

                }
                foreach (DataGridViewRow row in dgvValores.Rows)
                {
                    valorActual = Convert.ToDouble(row.Cells[0].Value);
                    double result = valorActual - _minValor;
                    frecObservada[(int)result] += 1;
                }
            }

            switch (_distribucion)
            {
                case "uniforme":
                    FrecEsperadaUniforme();
                    break;
                case "normal":
                    FrecEsperadaNormal(anchoIntervalo);
                    break;
                case "exponencial":
                    FrecEsperadaExpNegativa(anchoIntervalo);
                    break;
                case "poisson":
                    FrecEsperadaPoisson();
                    break;
            }

            double[] vectorFE = new double[_cantIntervalos];
            double[] vectorFO = new double[_cantIntervalos];
            double[] limites = new double[_cantIntervalos];
            double limiteIntervalo = _minValor + anchoIntervalo;

            double auxFE, auxFO;
            auxFE = 0;
            auxFO = 0;

            int position = 0;
            bool acumula = false;

            for (int i = 0; i < _cantIntervalos; i++)
            {

                if (frecEsperada[i] < 5 && i != (_cantIntervalos))
                {

                    auxFE += frecEsperada[i];
                    auxFO += frecObservada[i];
                    limiteIntervalo += anchoIntervalo;
                    acumula = true;
                }
                else if (frecEsperada[i] >= 5 && acumula)
                {
                    if (auxFE > 5)
                    {
                        vectorFE[position] = auxFE;
                        auxFE = 0;
                        vectorFO[position] = auxFO;
                        auxFO = 0;
                        limites[position] = limiteIntervalo;
                        position += 1;

                        acumula = false;

                        vectorFE[position] = frecEsperada[i];
                        vectorFO[position] = frecObservada[i];
                        limiteIntervalo += anchoIntervalo;
                        limites[position] = limiteIntervalo;
                        position += 1;
                    }
                    else
                    {
                        vectorFE[position] = frecEsperada[i] + auxFE;
                        auxFE = 0;
                        vectorFO[position] = frecObservada[i] + auxFO;
                        auxFO = 0;
                        limiteIntervalo += anchoIntervalo;
                        limites[position] = limiteIntervalo;
                        position += 1;
                    }
                }
                else if (frecEsperada[i] >= 5 && !acumula)
                {
                    vectorFE[position] = frecEsperada[i];
                    vectorFO[position] = frecObservada[i];
                    limiteIntervalo += anchoIntervalo;
                    limites[position] = limiteIntervalo;
                    position += 1;

                    acumula = false;
                }
                else if (index == (_cantIntervalos - 1))
                {
                    if (!acumula)
                    {
                        vectorFE[position] = frecEsperada[i];
                        vectorFO[position] = frecObservada[i];
                        limiteIntervalo += anchoIntervalo;
                        limites[position] = limiteIntervalo;
                    }
                    else if (frecEsperada[i] < 5 && acumula)
                    {
                        vectorFE[position] += frecEsperada[i] + auxFE;
                        auxFE = 0;
                        vectorFO[position] += frecObservada[i] + auxFO;
                        auxFO = 0;
                        limiteIntervalo += anchoIntervalo;
                        limites[position] = limiteIntervalo;
                    }
                    else
                    {
                        vectorFE[position] = auxFE;
                        auxFE = 0;
                        vectorFO[position] = auxFO;
                        auxFO = 0;
                        limiteIntervalo += anchoIntervalo;
                        limites[position] = limiteIntervalo;
                    }
                }
            }

            double[] vectorFESeries = new double[position];
            double[] vectorFOSeries = new double[position];

            for (int i = 0; i < position; i++)
            {
                vectorFESeries[i] = vectorFE[i];
                vectorFOSeries[i] = vectorFO[i];
            }

            double chiCalculado = 0;
            double desde, hasta;
            desde = _minValor;
            hasta = _minValor + anchoIntervalo;

            for (index = 0; index < vectorFESeries.Length; index++)
            {
                string m = Convert.ToString((hasta + desde)/2);
                chiCalculado += Math.Pow((vectorFOSeries[index] - vectorFESeries[index]), 2) / vectorFESeries[index];
                chrFrecuencias.Series["Frec. observada"].Points.AddXY(m, vectorFOSeries[index]);

                desde = hasta;
                hasta = desde + anchoIntervalo;
            }


            //seteo label
            lblValores.Text = "Cantidad de valores: ";
            lblIntervalos.Text = "Cantidad de intervalos: ";
            lblLibertad.Text = "Grados de libertad: ";
            lblTabulado.Text = "Chi Tabulado: ";
            lblCalculado.Text = "Chi Calculado: ";

            lblValores.Text += Convert.ToString(_cantValores);
            lblIntervalos.Text += Convert.ToString(vectorFESeries.Length);

            int gradosLibertad = vectorFE.Length;
            switch (_distribucion)
            {
                case "exponencial":
                    gradosLibertad -= 1;
                    break;
                case "normal":
                    gradosLibertad -= 2;
                    break;
                case "poisson":
                    gradosLibertad -= 1;
                    break;
            }


            lblLibertad.Text += Convert.ToString(gradosLibertad);
            lblTabulado.Text += Convert.ToString(vectorChi[gradosLibertad-1]);
            lblCalculado.Text += Convert.ToString(chiCalculado);

            string distribucion = "La serie generada corresponde a una distribución ";

            switch (_distribucion)
            {
                case "exponencial":
                    distribucion += "exponencial negativa.";
                    break;
                case "uniforme":
                    distribucion += "uniforme.";
                    break;
                case "normal":
                    distribucion += "normal.";
                    break;
                case "poisson":
                    distribucion += "de Poisson.";
                    break;
            }

            lblHipotesis.Text = distribucion;

            if (chiCalculado < vectorChi[_cantIntervalos-1])
            {
                picCheck.Visible = true;
                picCross.Visible = false;
            }
            else
            {
                picCross.Visible = true;
                picCheck.Visible = false;
            }

            this.ShowDialog();
        }

        private void DistribucionUniforme()
        {
            int count = 1;
            while (count <= _cantValores)
            {
                double aux = Convert.ToDouble(Math.Round(rnd.NextDouble(), 4));
                double? valor = DistUniformeAB(aux);

                if (valor < 1.79E+308 && valor > -1.79E+308)
                {
                    dgvValores.Rows.Add(valor);
                    count += 1;
                }
            }

            _maxValor = (double)_param2;
            _minValor = _param1;
        }

        private double? DistUniformeAB(double rnd)
        {
            return _param1 + rnd * (_param2 - _param1);
        }

        private void FrecEsperadaUniforme()
        {
            for (int index = 0; index < _cantIntervalos; index++)
            {
                double result = (_cantValores / _cantIntervalos);
                frecEsperada[index] = Math.Round(result, 4);
            }

        }

        public void cargarVectorChi()
        {
            vectorChi[0] = 3.8;
            vectorChi[1] = 6;
            vectorChi[2] = 7.8;
            vectorChi[3] = 9.5;
            vectorChi[4] = 11.1;
            vectorChi[5] = 12.6;
            vectorChi[6] = 14.1;
            vectorChi[7] = 15.5;
            vectorChi[8] = 16.9;
            vectorChi[9] = 18.3;
            vectorChi[10] = 19.7;
            vectorChi[11] = 21;
            vectorChi[12] = 22.4;
            vectorChi[13] = 23.7;
            vectorChi[14] = 25;
            vectorChi[15] = 26.3;
            vectorChi[16] = 27.6;
            vectorChi[17] = 28.9;
            vectorChi[18] = 30.1;
            vectorChi[19] = 31.4;
            vectorChi[20] = 32.7;
            vectorChi[21] = 33.9;
            vectorChi[22] = 35.2;
            vectorChi[23] = 36.4;
            vectorChi[24] = 37.7;
            vectorChi[25] = 38.9;
            vectorChi[26] = 40.1;
            vectorChi[27] = 41.3;
            vectorChi[28] = 42.6;
            vectorChi[29] = 43.8;

        }

        private void FrecEsperadaNormal(double anchoIntervalo)
        {
            double extInf, extSup, marcaClase, media, desvEst;

            extInf = _minValor;
            extSup = extInf + anchoIntervalo;
            marcaClase = Math.Round((extInf + extSup) / 2, 4);
            media = _param1;
            desvEst = (double)_param2;

            for (int index = 0; index < _cantIntervalos; index++)
            {
                frecEsperada[index] = Math.Round((Math.Exp(-0.5 * (Math.Pow((marcaClase - media) / desvEst, 2))) * 1 / (desvEst * Math.Sqrt(2 * Math.PI))) * anchoIntervalo * _cantValores, 4);

                extInf = extSup;
                extSup = extInf + anchoIntervalo;
                marcaClase = Math.Round((extInf + extSup) / 2, 4);
            }
        }
        private void Normal(double media, double desvEstandar)
        {
            double rnd1, rnd2, num1, num2;

            int count = 1;
            while (count <= _cantValores)
            {
                rnd1 = Convert.ToDouble(Math.Round(rnd.NextDouble(), 4));
                rnd2 = Convert.ToDouble(Math.Round(rnd.NextDouble(), 4));
                num1 = Math.Sqrt(-2 * Math.Log(rnd1)) * Math.Cos(2 * Math.PI * rnd2) * desvEstandar + media;
                num2 = Math.Sqrt(-2 * Math.Log(rnd1)) * Math.Sin(2 * Math.PI * rnd2) * desvEstandar + media;

                if (num1 < 1.79E+308 && num1 > -1.79E+308 && num2 < 1.79E+308 && num2 > -1.79E+308)
                {

                    dgvValores.Rows.Add(num1);
                    dgvValores.Rows.Add(num2);
                    count += 2;

                    if (num1 > num2)
                    {
                        if (num1 > _maxValor)
                            _maxValor = num1;
                        else if (num2 < _minValor)
                            _minValor = num2;
                    }
                    else
                    {
                        if (num2 > _maxValor)
                            _maxValor = num2;
                        else if (num1 < _minValor)
                            _minValor = num1;
                    }
                }

            }
            _minValor = Math.Round(_minValor, 4);
            _maxValor = Math.Round(_maxValor, 4);
        }

        private void ExpNegativa()
        {
            int count = 1;

            double auxRnd, media, valor;
            media = _param1;

            while (count <= _cantValores)
            {
                auxRnd = Convert.ToDouble(Math.Round(rnd.NextDouble(), 4));
                valor = DistExpNegativa(auxRnd, media);

                if (valor < 1.79E+308 && valor > -1.79E+308)
                {
                    dgvValores.Rows.Add(valor);
                    count += 1;

                    if (valor > _maxValor)
                        _maxValor = valor;
                    else if (valor < _minValor)
                        _minValor = valor;
                }
            }

            _minValor = Math.Round(_minValor, 4);
            _maxValor = Math.Round(_maxValor, 4);
        }

        private double DistExpNegativa(double auxRnd, double media)
        {
            return -(media) * Math.Log(1 - auxRnd);
        }


        private void FrecEsperadaExpNegativa(double ancho)
        {
            double extInf, extSup;

            extInf = _minValor;
            extSup = extInf + ancho;

            double lambda = 1 / _param1;

            for (int index = 0; index < _cantIntervalos; index++)
            {
                frecEsperada[index] = Math.Round(((1 - Math.Exp(-lambda * extSup)) - (1 - Math.Exp(-lambda * extInf))) * _cantValores, 4);

                extInf = extSup;
                extSup = extInf + ancho;

            }
        }

        private void Poisson()
        {
            int count =1;
            double valor, lambda;
            lambda = _param1;
            while (count <= _cantValores)
            {
                valor = DistPoisson(lambda);
                dgvValores.Rows.Add(valor);
                count += 1;

                if (valor > _maxValor)
                    _maxValor = valor;
                else if (valor < _minValor)
                    _minValor = valor;

            }

            _minValor = Math.Round(_minValor, 4);
            _maxValor = Math.Round(_maxValor, 4);

            int rango = ((int)_maxValor - (int)_minValor);
            if (rango < 30)
            {
                MessageBox.Show("A partir de la serie generada, la cantidad de intervalos se modificó a " + Convert.ToString(rango) + ".", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _cantIntervalos = rango;
            }


        }

        private double DistPoisson(double lambda)
        {
            int x;
            double p, a, aux;

            p = 1;
            x = -1;
            a = Math.Exp(-(lambda));

            while (p >= a)
            {
                aux = rnd.NextDouble();
                p *= aux;
                x += 1;
            }

            return x;
        }

        private void FrecEsperadaPoisson()
        {
            int valorActual;
            double t1, t2,t31, t4,t5;
            long t3;
            valorActual = Convert.ToInt32(_minValor);
            double lambda = _param1;
            for (int index = 0; index < _cantIntervalos; index++)
            {
                t1 = Math.Pow(lambda, valorActual);
                t2 = Math.Exp(-lambda);
                t3 = Factorial(valorActual);
                t31 = t1 * t2;
                t4 = (t31/ t3);
                t5 = t4 * _cantValores;
                frecEsperada[index] = Math.Round(t5, 0);

                valorActual += 1;
            }
        }

        private long Factorial(int number)
        {
            int resultado = 1;

            while (number >= 2)
            {
                resultado *= number;
                number -= 1;
            }

            return resultado;

        }
    }

}



