﻿namespace SimTp3
{
    partial class frmResultado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Label2 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.lblHipotesis = new System.Windows.Forms.Label();
            this.picCheck = new System.Windows.Forms.PictureBox();
            this.picCross = new System.Windows.Forms.PictureBox();
            this.lblLibertad = new System.Windows.Forms.Label();
            this.lblTabulado = new System.Windows.Forms.Label();
            this.lblCalculado = new System.Windows.Forms.Label();
            this.lblIntervalos = new System.Windows.Forms.Label();
            this.lblValores = new System.Windows.Forms.Label();
            this.dgvValores = new System.Windows.Forms.DataGridView();
            this.columnRnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chrFrecuencias = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCross)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrFrecuencias)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(31, 411);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(63, 13);
            this.Label2.TabIndex = 45;
            this.Label2.Text = "(agrupados)";
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.lblHipotesis);
            this.GroupBox1.Location = new System.Drawing.Point(858, 355);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(356, 84);
            this.GroupBox1.TabIndex = 44;
            this.GroupBox1.TabStop = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(137, 18);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(95, 18);
            this.Label1.TabIndex = 36;
            this.Label1.Text = "HIPÓTESIS";
            // 
            // lblHipotesis
            // 
            this.lblHipotesis.AutoSize = true;
            this.lblHipotesis.Location = new System.Drawing.Point(6, 50);
            this.lblHipotesis.Name = "lblHipotesis";
            this.lblHipotesis.Size = new System.Drawing.Size(347, 13);
            this.lblHipotesis.TabIndex = 35;
            this.lblHipotesis.Text = "La serie generada corresponde a una distribución exponencial negativa.";
            // 
            // picCheck
            // 
            this.picCheck.Image = global::SimTp3.Properties.Resources.check;
            this.picCheck.Location = new System.Drawing.Point(1240, 353);
            this.picCheck.Name = "picCheck";
            this.picCheck.Size = new System.Drawing.Size(90, 86);
            this.picCheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCheck.TabIndex = 43;
            this.picCheck.TabStop = false;
            this.picCheck.Visible = false;
            // 
            // picCross
            // 
            this.picCross.Image = global::SimTp3.Properties.Resources.cross;
            this.picCross.Location = new System.Drawing.Point(1240, 355);
            this.picCross.Name = "picCross";
            this.picCross.Size = new System.Drawing.Size(90, 86);
            this.picCross.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCross.TabIndex = 42;
            this.picCross.TabStop = false;
            this.picCross.Visible = false;
            // 
            // lblLibertad
            // 
            this.lblLibertad.AutoSize = true;
            this.lblLibertad.Location = new System.Drawing.Point(399, 355);
            this.lblLibertad.Name = "lblLibertad";
            this.lblLibertad.Size = new System.Drawing.Size(99, 13);
            this.lblLibertad.TabIndex = 41;
            this.lblLibertad.Text = "Grados de libertad: ";
            // 
            // lblTabulado
            // 
            this.lblTabulado.AutoSize = true;
            this.lblTabulado.Location = new System.Drawing.Point(399, 379);
            this.lblTabulado.Name = "lblTabulado";
            this.lblTabulado.Size = new System.Drawing.Size(76, 13);
            this.lblTabulado.TabIndex = 40;
            this.lblTabulado.Text = "Chi Tabulado: ";
            // 
            // lblCalculado
            // 
            this.lblCalculado.AutoSize = true;
            this.lblCalculado.Location = new System.Drawing.Point(397, 413);
            this.lblCalculado.Name = "lblCalculado";
            this.lblCalculado.Size = new System.Drawing.Size(78, 13);
            this.lblCalculado.TabIndex = 39;
            this.lblCalculado.Text = "Chi Calculado: ";
            // 
            // lblIntervalos
            // 
            this.lblIntervalos.AutoSize = true;
            this.lblIntervalos.Location = new System.Drawing.Point(12, 392);
            this.lblIntervalos.Name = "lblIntervalos";
            this.lblIntervalos.Size = new System.Drawing.Size(118, 13);
            this.lblIntervalos.TabIndex = 38;
            this.lblIntervalos.Text = "Cantidad de intervalos: ";
            // 
            // lblValores
            // 
            this.lblValores.AutoSize = true;
            this.lblValores.Location = new System.Drawing.Point(12, 364);
            this.lblValores.Name = "lblValores";
            this.lblValores.Size = new System.Drawing.Size(107, 13);
            this.lblValores.TabIndex = 37;
            this.lblValores.Text = "Cantidad de valores: ";
            // 
            // dgvValores
            // 
            this.dgvValores.AllowUserToAddRows = false;
            this.dgvValores.AllowUserToDeleteRows = false;
            this.dgvValores.AllowUserToResizeColumns = false;
            this.dgvValores.AllowUserToResizeRows = false;
            this.dgvValores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvValores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvValores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnRnd});
            this.dgvValores.Location = new System.Drawing.Point(12, 25);
            this.dgvValores.Name = "dgvValores";
            this.dgvValores.Size = new System.Drawing.Size(218, 300);
            this.dgvValores.TabIndex = 36;
            // 
            // columnRnd
            // 
            this.columnRnd.FillWeight = 70F;
            this.columnRnd.HeaderText = "Valores aleatorios";
            this.columnRnd.Name = "columnRnd";
            this.columnRnd.ReadOnly = true;
            // 
            // chrFrecuencias
            // 
            chartArea1.Name = "ChartArea1";
            this.chrFrecuencias.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chrFrecuencias.Legends.Add(legend1);
            this.chrFrecuencias.Location = new System.Drawing.Point(236, 25);
            this.chrFrecuencias.Name = "chrFrecuencias";
            series1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.VerticalCenter;
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Frec. observada";
            this.chrFrecuencias.Series.Add(series1);
            this.chrFrecuencias.Size = new System.Drawing.Size(1094, 300);
            this.chrFrecuencias.TabIndex = 46;
            this.chrFrecuencias.Text = "chart1";
            // 
            // frmResultado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 460);
            this.Controls.Add(this.chrFrecuencias);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.picCheck);
            this.Controls.Add(this.picCross);
            this.Controls.Add(this.lblLibertad);
            this.Controls.Add(this.lblTabulado);
            this.Controls.Add(this.lblCalculado);
            this.Controls.Add(this.lblIntervalos);
            this.Controls.Add(this.lblValores);
            this.Controls.Add(this.dgvValores);
            this.Name = "frmResultado";
            this.Text = "frmResultado";
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCross)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chrFrecuencias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label lblHipotesis;
        internal System.Windows.Forms.PictureBox picCheck;
        internal System.Windows.Forms.PictureBox picCross;
        internal System.Windows.Forms.Label lblLibertad;
        internal System.Windows.Forms.Label lblTabulado;
        internal System.Windows.Forms.Label lblCalculado;
        internal System.Windows.Forms.Label lblIntervalos;
        internal System.Windows.Forms.Label lblValores;
        internal System.Windows.Forms.DataGridView dgvValores;
        internal System.Windows.Forms.DataGridViewTextBoxColumn columnRnd;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrFrecuencias;
    }
}