﻿namespace SimTp3
{
    partial class frmParametros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.grpDatos = new System.Windows.Forms.GroupBox();
            this.txtIntervalos = new System.Windows.Forms.TextBox();
            this.lblIntervalos = new System.Windows.Forms.Label();
            this.txtValores = new System.Windows.Forms.TextBox();
            this.lblValores = new System.Windows.Forms.Label();
            this.grpParametros = new System.Windows.Forms.GroupBox();
            this.txtParam2 = new System.Windows.Forms.TextBox();
            this.txtParam1 = new System.Windows.Forms.TextBox();
            this.lblParam1 = new System.Windows.Forms.Label();
            this.lblParam2 = new System.Windows.Forms.Label();
            this.grpDatos.SuspendLayout();
            this.grpParametros.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(20, 245);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 2;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(101, 245);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click_1);
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(182, 245);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(75, 23);
            this.btnGenerar.TabIndex = 6;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // grpDatos
            // 
            this.grpDatos.BackColor = System.Drawing.Color.Transparent;
            this.grpDatos.Controls.Add(this.txtIntervalos);
            this.grpDatos.Controls.Add(this.lblIntervalos);
            this.grpDatos.Controls.Add(this.txtValores);
            this.grpDatos.Controls.Add(this.lblValores);
            this.grpDatos.Location = new System.Drawing.Point(26, 129);
            this.grpDatos.Name = "grpDatos";
            this.grpDatos.Size = new System.Drawing.Size(234, 100);
            this.grpDatos.TabIndex = 5;
            this.grpDatos.TabStop = false;
            this.grpDatos.Text = "Valores/Intervalos";
            // 
            // txtIntervalos
            // 
            this.txtIntervalos.Location = new System.Drawing.Point(128, 60);
            this.txtIntervalos.Name = "txtIntervalos";
            this.txtIntervalos.Size = new System.Drawing.Size(100, 20);
            this.txtIntervalos.TabIndex = 5;
            // 
            // lblIntervalos
            // 
            this.lblIntervalos.AutoSize = true;
            this.lblIntervalos.Location = new System.Drawing.Point(6, 63);
            this.lblIntervalos.Name = "lblIntervalos";
            this.lblIntervalos.Size = new System.Drawing.Size(112, 13);
            this.lblIntervalos.TabIndex = 5;
            this.lblIntervalos.Text = "Cantidad de intervalos";
            // 
            // txtValores
            // 
            this.txtValores.Location = new System.Drawing.Point(128, 31);
            this.txtValores.Name = "txtValores";
            this.txtValores.Size = new System.Drawing.Size(100, 20);
            this.txtValores.TabIndex = 4;
            // 
            // lblValores
            // 
            this.lblValores.AutoSize = true;
            this.lblValores.Location = new System.Drawing.Point(6, 34);
            this.lblValores.Name = "lblValores";
            this.lblValores.Size = new System.Drawing.Size(101, 13);
            this.lblValores.TabIndex = 4;
            this.lblValores.Text = "Cantidad de valores";
            // 
            // grpParametros
            // 
            this.grpParametros.BackColor = System.Drawing.Color.Transparent;
            this.grpParametros.Controls.Add(this.txtParam2);
            this.grpParametros.Controls.Add(this.txtParam1);
            this.grpParametros.Controls.Add(this.lblParam1);
            this.grpParametros.Controls.Add(this.lblParam2);
            this.grpParametros.Location = new System.Drawing.Point(26, 24);
            this.grpParametros.Name = "grpParametros";
            this.grpParametros.Size = new System.Drawing.Size(234, 97);
            this.grpParametros.TabIndex = 4;
            this.grpParametros.TabStop = false;
            this.grpParametros.Text = "Parámetros";
            // 
            // txtParam2
            // 
            this.txtParam2.Location = new System.Drawing.Point(128, 56);
            this.txtParam2.Name = "txtParam2";
            this.txtParam2.Size = new System.Drawing.Size(100, 20);
            this.txtParam2.TabIndex = 3;
            // 
            // txtParam1
            // 
            this.txtParam1.Location = new System.Drawing.Point(128, 27);
            this.txtParam1.Name = "txtParam1";
            this.txtParam1.Size = new System.Drawing.Size(100, 20);
            this.txtParam1.TabIndex = 2;
            // 
            // lblParam1
            // 
            this.lblParam1.AutoSize = true;
            this.lblParam1.Location = new System.Drawing.Point(6, 30);
            this.lblParam1.Name = "lblParam1";
            this.lblParam1.Size = new System.Drawing.Size(64, 13);
            this.lblParam1.TabIndex = 0;
            this.lblParam1.Text = "Parámetro 1";
            // 
            // lblParam2
            // 
            this.lblParam2.AutoSize = true;
            this.lblParam2.Location = new System.Drawing.Point(6, 59);
            this.lblParam2.Name = "lblParam2";
            this.lblParam2.Size = new System.Drawing.Size(64, 13);
            this.lblParam2.TabIndex = 1;
            this.lblParam2.Text = "Parámetro 2";
            // 
            // frmParametros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 296);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGenerar);
            this.Controls.Add(this.grpDatos);
            this.Controls.Add(this.grpParametros);
            this.Controls.Add(this.btnLimpiar);
            this.Name = "frmParametros";
            this.Text = "frmParametros";
            this.grpDatos.ResumeLayout(false);
            this.grpDatos.PerformLayout();
            this.grpParametros.ResumeLayout(false);
            this.grpParametros.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLimpiar;
        internal System.Windows.Forms.Button btnCancelar;
        internal System.Windows.Forms.Button btnGenerar;
        internal System.Windows.Forms.GroupBox grpDatos;
        internal System.Windows.Forms.TextBox txtIntervalos;
        internal System.Windows.Forms.Label lblIntervalos;
        internal System.Windows.Forms.TextBox txtValores;
        internal System.Windows.Forms.Label lblValores;
        internal System.Windows.Forms.GroupBox grpParametros;
        internal System.Windows.Forms.TextBox txtParam2;
        internal System.Windows.Forms.TextBox txtParam1;
        internal System.Windows.Forms.Label lblParam1;
        internal System.Windows.Forms.Label lblParam2;
    }
}