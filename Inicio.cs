﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimTp3
{
    public partial class Inicio : Form
    {

        public frmParametros parametros;

        public Inicio()
        {
            InitializeComponent();
            parametros = new frmParametros();
        }


        private void btnUniforme_Click(object sender, EventArgs e)
        {
            parametros.Text = "Dist. Uniforme (a, b)";
            parametros.lblParam1.Text = "Extremo inferior (a)";
            parametros.lblParam2.Text = "Extremo superior (b)";
            parametros.Tag = "uniforme";
            parametros.txtIntervalos.Enabled = true;
            parametros.txtIntervalos.Text = "";
            parametros.ShowDialog();
        }

        private void btnNormal_Click(object sender, EventArgs e)
        {
            parametros.Text = "Distribución Normal";
            parametros.lblParam1.Text = "Media (μ)";
            parametros.lblParam2.Text = "Desv. Estándar (σ)";
            parametros.Tag = "normal"; 
            parametros.txtIntervalos.Enabled = true;
            parametros.txtIntervalos.Text = "";
            parametros.ShowDialog();
        }

        private void btnExponencial_Click(object sender, EventArgs e)
        {
            parametros.Text = "Dist. Exp. Negativa";
            parametros.lblParam1.Text = "Media (μ)";
            parametros.lblParam2.Text = "Relación entre media y frecuencia: μ = 1/λ";
            parametros.txtParam2.Visible = false;
            parametros.txtIntervalos.Enabled = true;
            parametros.txtIntervalos.Text = "";
            parametros.Tag = "exponencial";
            parametros.ShowDialog();
        }

        private void btnPoisson_Click(object sender, EventArgs e)
        {
            parametros.Text = "Dist. de Poisson";
            parametros.lblParam1.Text = "Frecuencia (λ)";
            parametros.lblParam2.Text = "El algoritmo es ineficiente si λ es muy grande.";
            parametros.txtParam2.Visible = false;
            parametros.txtIntervalos.Enabled = false;
            parametros.txtIntervalos.Text = "2";
            parametros.Tag = "poisson";
            parametros.ShowDialog();
        }
    }
}
