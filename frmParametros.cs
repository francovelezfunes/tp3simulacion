﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimTp3
{
    public partial class frmParametros : Form
    {
        public frmParametros()
        {
            InitializeComponent();
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que quiere salir?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                this.Close();
            }
        }


        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que quiere salir?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                this.Close();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            if (txtParam1.Text == "" || (txtParam2.Visible == true && txtParam2.Text == "") || txtValores.Text == "" || (txtIntervalos.Text == "" && txtIntervalos.Enabled == true))
            {
                MessageBox.Show("Debe completar todos los campos.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrEmpty(txtParam2.Text)) txtParam2.Text = "0";
            new frmResultado(Convert.ToDouble(txtParam1.Text), Convert.ToInt32(txtParam2.Text), Convert.ToInt32(txtValores.Text), Convert.ToInt32(txtIntervalos.Text), this.Tag.ToString());

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtValores.Text = "";
            txtParam1.Text = "";
            txtParam2.Text = "";
            if (this.Tag.ToString() == "poisson") txtIntervalos.Text = "2";
            else txtIntervalos.Text = "";
        }
    }
}
